package com.hellokoding.jpa.repository;

import com.hellokoding.jpa.model.Stock;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StockRepository extends JpaRepository<Stock, Integer> {
}
